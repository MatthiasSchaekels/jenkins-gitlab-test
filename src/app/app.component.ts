import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public vampireWeekend : string;
  public displaythis : number;

  private _stand : string;

  constructor(){
    // let test: string = 14;
    let test : number = 10;
    let mooglie = test * test;
    this.displaythis = mooglie;

  }

  get stand(){
    return this._stand;
  }
}
